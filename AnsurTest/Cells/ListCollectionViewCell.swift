//
//  ListCollectionViewCell.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import UIKit
import AlamofireImage

class ListCollectionViewCell: UICollectionViewCell, ReusableView, NibLoadable {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupImage(image: String) {
        if let url = URL(string: image) {
            
            self.contentView.layoutIfNeeded()
            
            // add loading overlay
            let overlay = UIView(frame: CGRect(origin: CGPoint.zero, size: self.contentView.frame.size))
            overlay.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
            self.contentView.addSubview(overlay)
            
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.startAnimating()
            self.contentView.addSubview(activityIndicator)
            
            self.contentView.addConstraints([activityIndicator.centerX(to: self.contentView),
                                             activityIndicator.centerY(to: self.contentView)])
            
            self.contentView.bringSubview(toFront: imageView)
            imageView.image = nil
            imageView.af_setImage(withURL: url) { (_) in
                overlay.removeFromSuperview()
            }
        }
    }

}
