//
//  UIView+LayoutConstraints.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import UIKit

extension UIView {
    func centerX(to view: UIView) -> NSLayoutConstraint {
        return NSLayoutConstraint.init(item: self, attribute: .centerX,
                                       relatedBy: .equal, toItem: view,
                                       attribute: .centerX,
                                       multiplier: 1, constant: 0)
    }
    
    func centerY(to view: UIView) -> NSLayoutConstraint {
        return NSLayoutConstraint.init(item: self, attribute: .centerY,
                                       relatedBy: .equal, toItem: view,
                                       attribute: .centerY,
                                       multiplier: 1, constant: 0)
    }
}
