//
//  APIManager.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import Foundation
import Alamofire

enum APIManagerError {
    case none
    case serverError(status: Int)
    case jsonError
}

extension HTTPURLResponse {
    
    func isSuccess() -> Bool {
        if 200..<300 ~= self.statusCode {
            return true
        }
        
        return false
    }
}

class APIManager {
    
    typealias ListCallback = ([Image], APIManagerError) -> Void
    
    private let username = "testcandidate"
    private let password = "bygcd33"
    
    let baseURL: URL = URL(string: "https://thor.ansur.no/api")!
    
    func list(limit: Int, callback: @escaping ListCallback) {
        
        let endpoint = baseURL.appendingPathComponent("/photos.json?limit=\(limit)")
        
        Alamofire.request(endpoint)
            .authenticate(user: username, password: password)
            .responseJSON { response in
                
                if !response.response!.isSuccess() {
                    callback([], .serverError(status: response.response!.statusCode))
                } else {
                    
                    if let json = response.result.value as? [String: Any],
                        let data = json["data"] as? [String: Any] {
                        
                        let photos = data["photos"] as! [[String: Any]]
                        var images: [Image] = []
                        
                        for photo in photos {
                            images.append(Image(from: photo))
                        }
                        
                        callback(images, .none)
                    } else {
                        callback([], .jsonError)
                    }
                    
                }
                
        }
        
    }
    
}





