//
//  Image.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import Foundation
import CoreLocation

struct Image {
    let id: Int
    let previewURL: String
    let thumbURL: String
    let missionName: String
    let location: CLLocation?
    
    init(from json: [String: Any]) {
        self.id = json["id"] as? Int ?? 0
        self.previewURL = json["preview_url"] as? String ?? ""
        self.thumbURL = json["thumbnail_url"] as? String ?? ""
        self.missionName = json["mission_name"] as? String ?? ""
        
        if let latitude = json["location_latitude"] as? String,
            let longitude = json["location_longitude"] as? String {
            self.location = CLLocation(latitude: Double(latitude)!, longitude: Double(longitude)!)
        } else {
            self.location = nil
        }
    }

}
