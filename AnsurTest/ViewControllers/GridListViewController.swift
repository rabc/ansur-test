//
//  GridListViewController.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import UIKit

class GridListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var listCollectionView: UICollectionView!
    
    let viewModel = GridListViewModel()
    
    var gravity: UIGravityBehavior?
    var animator: UIDynamicAnimator?
    var collision: UICollisionBehavior?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animator = UIDynamicAnimator(referenceView: self.view)
        
        let layout = listCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10)
        
        listCollectionView.register(cell: ListCollectionViewCell.self)
        
        viewModel.listImages { [unowned self] (success) in
            if success {
                
                self.listCollectionView.reloadData()
                
            } else {
                let alertController = UIAlertController(title: "Warning",
                                                        message: "An error occurred with the server. Please, try again later.",
                                                        preferredStyle: .alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default)
                alertController.addAction(OKAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func didTapView(gesture: UITapGestureRecognizer) {
        guard collision != nil else {
            return
        }
        
        guard (collision!.items.first as! UIView).frame.contains(gesture.location(in: self.view)) == false else {
            return
        }
        
        self.view.removeGestureRecognizer(gesture)
        
        collision!.removeAllBoundaries()
        collision = nil
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfImages()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ListCollectionViewCell = listCollectionView.dequeueReusableCell(withReuseIdentifier: ListCollectionViewCell.reuseIdentifier, for: indexPath) as! ListCollectionViewCell
        
        cell.setupImage(image: viewModel.image(at: indexPath.row))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let detailView = viewModel.detailView(index: indexPath.row)
        self.view.addSubview(detailView)
        
        // center it
        self.view.addConstraints([detailView.centerX(to: self.view),
                                  NSLayoutConstraint.init(item: detailView, attribute: .bottom,
                                                          relatedBy: .equal, toItem: self.view,
                                                          attribute: .top,
                                                          multiplier: 1, constant: 0)])
        self.view.layoutIfNeeded()
        
        if gravity != nil {
            animator!.removeAllBehaviors()
        }
        
        // Animate
        gravity = UIGravityBehavior(items: [detailView])
        animator!.addBehavior(gravity!)
        
        let lowerLimit: CGFloat = detailView.frame.size.height + 50
        
        collision = UICollisionBehavior(items: [detailView])
        collision!.addBoundary(withIdentifier: "limit" as NSCopying,
                               from: CGPoint(x: self.view.frame.origin.x, y: lowerLimit),
                               to: CGPoint(x: self.view.frame.origin.x + self.view.frame.width, y: lowerLimit))
        animator!.addBehavior(collision!)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        self.view.addGestureRecognizer(tapGesture)
        
    }

}
