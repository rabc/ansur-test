//
//  GridListViewModel.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import UIKit
import MapKit
import AlamofireImage

class GridListViewModel {
    
    var images: [Image] = []
    
    private var manager = APIManager()
    
    let limit: Int
    let thumbSize: CGSize = CGSize(width: 150, height: 150)
    
    init(limit: Int = 50) {
        self.limit = limit
    }
    
    /// Dependecy injection init for testing
    ///
    /// - Parameter manager:
    init(manager: APIManager, limit: Int = 50) {
        self.manager = manager
        self.limit = limit
    }
    
    func listImages(_ callback: @escaping (Bool) -> Void) {
        manager.list(limit: limit) { [weak self] (images, error) in
            
            if case .none = error {
                self?.images = images
                callback(true)
            } else {
                callback(false)
            }
            
        }
    }
    
    func numberOfImages() -> Int {
        return images.count
    }
    
    func image(at index: Int) -> String {
        guard images.count > index else {
            return ""
        }
        
        return images[index].thumbURL
    }
    
    func coordinate(at index: Int) -> CLLocationCoordinate2D? {
        guard images.count > index else {
            return nil
        }
        
        return images[index].location?.coordinate ?? nil
    }
    
    func missionName(at index: Int) -> String {
        guard images.count > index else {
            return ""
        }
        
        return images[index].missionName
    }
    
    func detailView(index: Int) -> UIView {
        // 80% of device size
        let squareSize = UIScreen.main.bounds.size.width * 0.8
        let size = CGSize(width: squareSize, height: squareSize)
        
        let detailView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: size.width, height: size.height)))
        detailView.translatesAutoresizingMaskIntoConstraints = false
        detailView.backgroundColor = UIColor.white
        detailView.addConstraints([NSLayoutConstraint.init(item: detailView, attribute: .width,
                                                           relatedBy: .equal, toItem: nil,
                                                           attribute: .notAnAttribute,
                                                           multiplier: 1, constant: squareSize),
                                   NSLayoutConstraint.init(item: detailView, attribute: .height,
                                                           relatedBy: .equal, toItem: nil,
                                                           attribute: .notAnAttribute,
                                                           multiplier: 1, constant: squareSize)])
        
        if let coordinate = coordinate(at: index) {
            // map
            let mapView = MKMapView(frame: detailView.frame)
            
            // pin
            let pin = MKPointAnnotation()
            pin.coordinate = coordinate
            pin.title = missionName(at: index)
            mapView.addAnnotation(pin)
            mapView.setRegion(MKCoordinateRegionMake(pin.coordinate, MKCoordinateSpanMake(0.75, 0.75)), animated: false)
            
            detailView.addSubview(mapView)
        } else {
            
            let label = UILabel(frame: detailView.frame)
            label.textAlignment = .center
            label.text = "Coordinate not available"
            
            detailView.addSubview(label)
            
        }
        
        return detailView
    }
    
}
