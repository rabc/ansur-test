//
//  APIManagerTests.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import XCTest
import OHHTTPStubs
@testable import AnsurTest

class APIManagerTests: XCTestCase {
    
    let timeout: TimeInterval = 5.0
    
    var response: [String: Any] = [:]
    
    override func setUp() {
        super.setUp()
        
        do {
            let file = Bundle(for: type(of: self)).url(forResource: "api.response", withExtension: "json")!
            let data = try Data(contentsOf: file)
            if let rawJsonConfig = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                response = rawJsonConfig
            }
            
        } catch {
            fatalError("Unable to read response json")
        }
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBaseURL() {
        let manager = APIManager()
        XCTAssertEqual(manager.baseURL.absoluteString, "https://thor.ansur.no/api")
    }
    
    func testList_success() {
        let exp = expectation(description: #function)
        let limit = 5
        
        stub(condition: isHost("thor.ansur.no")) { (request) -> OHHTTPStubsResponse in
            XCTAssertEqual(request.httpMethod, "GET")
            XCTAssertEqual(request.url?.lastPathComponent, "photos.json?limit=\(limit)")
            
            let data = try! JSONSerialization.data(withJSONObject: self.response, options: [])
            return OHHTTPStubsResponse(data: data, statusCode: 200, headers:nil)
        }
        
        let manager = APIManager()
        manager.list(limit: limit) { (images, error) in
            if case .none = error {
                XCTAssertTrue(true)
            } else {
                XCTFail("Should return no error")
            }
            
            exp.fulfill()
            
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
        
    }
    
    func testList_fail() {
        let errorStatus = 500
        stub(condition: isHost("thor.ansur.no")) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(data: Data(), statusCode: Int32(errorStatus), headers:nil)
        }
        
        let exp = expectation(description: #function)
        
        let manager = APIManager()
        manager.list(limit: 1) { (images, error) in
            if case .serverError(let status) = error {
                XCTAssertTrue(status == errorStatus)
            } else {
                XCTFail("Should return serverError")
            }
            
            XCTAssertTrue(images.count == 0)
            
            exp.fulfill()
            
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
        
    }
    
}
