//
//  ImageTests.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import XCTest
@testable import AnsurTest

class ImageTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitDict() {
        let dict: [String: Any] = ["id": 1, "preview_url": "http://ansur.no",
                                   "thumbnail_url": "http://ansur.no/",
                                   "mission_name": "Mission X",
                                   "location_latitude": "50.8241831251956",
                                   "location_longitude": "-0.175480842565904"]
        
        let image = Image(from: dict)
        
        XCTAssertEqual(image.id, dict["id"] as! Int)
        XCTAssertEqual(image.previewURL, dict["preview_url"] as! String)
        XCTAssertEqual(image.thumbURL, dict["thumbnail_url"] as! String)
        XCTAssertEqual(image.missionName, dict["mission_name"] as! String)
        XCTAssertNotNil(image.location)
        XCTAssertEqual(image.location?.coordinate.latitude, Double(dict["location_latitude"] as! String))
        XCTAssertEqual(image.location?.coordinate.longitude, Double(dict["location_longitude"] as! String))
    }
}
