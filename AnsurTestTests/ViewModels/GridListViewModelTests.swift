//
//  GridListViewModelTests.swift
//  AnsurTest
//
//  Created by Ricardo Borelli on 26.05.17.
//
//

import XCTest
@testable import AnsurTest

class MockSuccessAPIManager: APIManager {
    var limit: Int?
    
    override func list(limit: Int, callback: @escaping APIManager.ListCallback) {
        self.limit = limit
        
        let dict: [String: Any] = ["id": 1, "preview_url": "http://ansur.no",
                                   "thumbnail_url": "http://ansur.no/",
                                   "mission_name": "Mission X",
                                   "location_latitude": "50.8241831251956",
                                   "location_longitude": "-0.175480842565904"]
        let image = Image(from: dict)
        callback([image], .none)
    }
}

class MockFailAPIManager: APIManager {
    var limit: Int?
    
    override func list(limit: Int, callback: @escaping APIManager.ListCallback) {
        self.limit = limit
        
        callback([], .serverError(status: 500))
    }
}

class GridListViewModelTests: XCTestCase {
    
    let timeout: TimeInterval = 5.0
    var image: Image!
    
    override func setUp() {
        super.setUp()
        
        let dict: [String: Any] = ["id": 1, "preview_url": "http://ansur.no",
                                   "thumbnail_url": "http://ansur.no/",
                                   "mission_name": "Mission X",
                                   "location_latitude": "50.8241831251956",
                                   "location_longitude": "-0.175480842565904"]
        image = Image(from: dict)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit_default() {
        let viewModel = GridListViewModel()
        XCTAssertEqual(viewModel.limit, 50)
        XCTAssertEqual(viewModel.thumbSize, CGSize(width: 150, height: 150))
    }
    
    func testInit_limit() {
        let viewModel = GridListViewModel(limit: 1)
        XCTAssertEqual(viewModel.limit, 1)
    }
    
    func testNumberOfImages() {
        let viewModel = GridListViewModel()
        
        viewModel.images = [image]
        
        XCTAssertTrue(viewModel.numberOfImages() == 1)
    }
    
    func testImage() {
        let viewModel = GridListViewModel()
        viewModel.images = [image]
        
        XCTAssertEqual(viewModel.image(at: 0), image.thumbURL)
    }
    
    func testMissionName() {
        let viewModel = GridListViewModel()
        viewModel.images = [image]
        
        XCTAssertEqual(viewModel.missionName(at: 0), image.missionName)
    }
    
    func testCoordinate() {
        let viewModel = GridListViewModel()
        viewModel.images = [image]
        
        XCTAssertEqual(viewModel.coordinate(at: 0)?.latitude, image.location?.coordinate.latitude)
        XCTAssertEqual(viewModel.coordinate(at: 0)?.longitude, image.location?.coordinate.longitude)
    }
    
    func testList_success() {
        let exp = expectation(description: #function)
        
        let manager = MockSuccessAPIManager()
        let viewModel = GridListViewModel(manager: manager)
        
        viewModel.listImages { (success) in
            XCTAssertTrue(success)
            XCTAssertEqual(manager.limit, 50)
            XCTAssertTrue(viewModel.images.count > 0)
            exp.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testList_fail() {
        let exp = expectation(description: #function)
        
        let manager = MockFailAPIManager()
        let viewModel = GridListViewModel(manager: manager)
        
        viewModel.listImages { (success) in
            XCTAssertFalse(success)
            XCTAssertTrue(viewModel.images.count == 0)
            exp.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testDetailView() {
        let viewModel = GridListViewModel()
        viewModel.images = [image]
        
        let view = viewModel.detailView(index: 0)
        
        XCTAssertEqual(view.backgroundColor, UIColor.white)
        XCTAssertTrue(view.subviews.count == 1)
    }
    
}
